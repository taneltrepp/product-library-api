const Joi = require('joi');

module.exports = {
    validateParam: (schema, name) => {
        return (req, res, next) => {
            const result = Joi.validate({ param: req['params'][name] }, schema);
            if (result.error) {
                return res.status(400).json(result.error);
            } else {
                if (!req.value)
                    req.value = {};

                if (!req.value['params'])
                    req.value['params'] = {};

                req.value['params'][name] = result.value.param;
                next();
            }
        }
    },

    validateBody: (schema) => {
        return (req, res, next) => {
            const result = Joi.validate(req.body, schema);

            if (result.error) {
                return res.status(400).json(result.error);
            } else {
                if (!req.value)
                    req.value = {};

                if (!req.value['body'])
                    req.value['body'] = {};

                req.value['body'] = result.value;
                next();
            }
        }
    },

    schemas: {
        categorySchema: Joi.object().keys({
            categoryType: Joi.string().required()
        }),

        categoryOptionalSchema: Joi.object().keys({
            categoryType: Joi.string()
        }),

        productSchema: Joi.object().keys({
            categoryType: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
            productName: Joi.string().required(),
            price: Joi.number().required()
        }),

        patchProductSchema: Joi.object().keys({
            productName: Joi.string(),
            price: Joi.number()
        }),

        idSchema: Joi.object().keys({
            param: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        })
    }
}

function validateParam() {

}
