const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/productsapi', { useNewUrlParser: true, useUnifiedTopology: true }, () => {
console.log('connected to mongodb');
})

const app = express();

const products = require('./routes/products');
const categories = require('./routes/categories');

app.use(logger('dev'));
app.use(bodyParser.json());

app.use('/products', products)
app.use('/categories', categories)

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;

    res.status(status).json({
        error: {
            message: error.message
        }
    });
    console.error(err);
});

const port = app.get('port') || 3000;
app.listen(port, () => console.log(`Server is connected to port ${port}`));