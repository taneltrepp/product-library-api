const router = require('express-promise-router')();

const ProductsController = require('../controllers/products');

const {
    validateBody,
    validateParam,
    schemas
} = require('../helpers/routeHelpers');

router.route('/')
    .post(validateBody(schemas.productSchema),
        ProductsController.newProduct);
router.route('/:productId')
    .patch([validateParam(schemas.idSchema, 'productId'), validateBody(schemas.patchProductSchema)],
        ProductsController.updateProduct)
    .delete(validateParam(schemas.idSchema, 'productId'), 
    ProductsController.deleteProduct)

module.exports = router;