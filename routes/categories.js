const express = require('express');
const router = require('express-promise-router')();

const CategoriesController = require('../controllers/categories');

const { validateParam, validateBody, schemas } = require('../helpers/routeHelpers');

router.route('/')
    .get(CategoriesController.index)
    .post(validateBody(schemas.categorySchema), CategoriesController.newCategory);

router.route('/:categoryId')
    .patch([validateParam(schemas.idSchema, 'categoryId'), validateBody(schemas.categoryOptionalSchema)],
        CategoriesController.updateCategory)
    .delete(validateParam(schemas.idSchema, 'categoryId'),
        CategoriesController.deleteCategory);

router.route('/:categoryId/products')
    .get(validateParam(schemas.idSchema, 'categoryId'), CategoriesController.getCategoryProducts);

module.exports = router;