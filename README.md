# Assignment

REST API for catalog of products. Contains categories and products. One product can belong to only one category.

# Lets get started

To run this program, install these programs:
- visual studio code
- node.js
- mongoDB Compass
- Postman
- install npm

# After installations

# Set up the server with MongoDB Compass
Open mongoDB Compass and create a database called "productapi" and add two collections "categories" and "products". Insert JSON files from the db folder.

# Set up the server with Visual Studio Code
Open Visual studio code and the install these dependencies by opening the terminal and typing in:

"npm init -y"

"npm i body-parser express express-promise-router joi mongoose morgan"

After that your ready to start your server by typing in the terminal: 

"npm run devStart"

# Test the functionality with Postman
For better oversight, create a new collection.

•	To get all categories: use GET request and type  "http://localhost:3000/categories" inside the Enter request URL tab.

•	To get the products of a certain category: use GET request and type "http://localhost:3000/categories/<category id>/products" inside the Enter request URL tab and replace <category id>, with the preferred category id, provided from the get all categories request.

•	To create a category or product: use POST request and type "http://localhost:3000/categories" or "http://localhost:3000/products" inside the Enter request URL tab. For this request you have to fill out the body, by clicking body, then RAW and then replace Text input with JSON. Enter the request in JSON format.
Categories have only one parameter:

{ 
    "categoryType": "Your category"
}

Products have to have the desired category too:

{
	"productName": "Nokia 3110",
	"price": 10,
	"categoryType": "5e8d98dcc225d205f513a873"
}

•	To Update a category or product: use PATCH request and type "http://localhost:3000/categories/<id>" or "http://localhost:3000/products/<id>" inside the Enter request URL tab (whichever you want to update), also replace "<id>" with the id of the element you wish to update. For this request you have to fill out the body aswell, by clicking body, then RAW and then replace Text input with JSON. Enter the request in JSON format.

Categories have only one parameter:

{ "categoryType": "Your category"}

Products have to include the categoryType (id):

{
	"productName": "Nokia 3110",
	"price": 10,
	"categoryType": "5e8d98dcc225d205f513a873"
}

•	To delete a category or product: use DELETE request and type "http://localhost:3000/categories/<id>" or "http://localhost:3000/products/<id>" inside the Enter request URL tab (whichever you want to delete), also replace "<id>" with the id of the element you wish to delete.
