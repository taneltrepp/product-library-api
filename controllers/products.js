const Product = require('../models/product');
const Category = require('../models/category');

module.exports = {
    newProduct: async (req, res, next) => {
        const categoryType = await Category.findById(req.value.body.categoryType);

        const newProduct = req.value.body;
        delete newProduct.categoryType;
        const product = new Product(newProduct);
        product.categoryType = categoryType;
        await product.save();

        categoryType.products.push(product);
        await categoryType.save();
        res.status(200).json(product);
    },
    updateProduct: async (req, res, next) => {
        const product = await Product.findByIdAndUpdate(req.value.params.productId, req.value.body);
        res.status(200).json({ success: true});
    },
    deleteProduct: async (req, res, next) => {

        const product = await Product.findById(req.value.params.productId);
        if (!product) {
            return res.status(404).json({ error: 'Product doesn\'t exist' });
        }

        const categoryType = await Category.findById(product.categoryType);
        await product.remove();
        categoryType.products.pull(product);
        await categoryType.save();

        res.status(200).json({ success: true });
    }
}