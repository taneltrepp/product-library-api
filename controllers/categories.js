const Category = require('../models/category');
const Product = require('../models/product')


module.exports = {
    index: async (req, res, next) => {
        const categories = await Category.find({});
        res.status(200).json(categories);
    },
    newCategory: async (req, res, next) => {
        const newCategory = new Category(req.value.body);
        const category = await newCategory.save();

        res.status(201).json(category);
    },
    updateCategory: async (req, res, next) => {
        const category = await Category.findByIdAndUpdate(req.value.params.categoryId, req.value.body);
        res.status(200).json({ success: true });
    },
    getCategoryProducts: async (req, res, next) => {
        const category = await Category.findById(req.value.params.categoryId).populate({
            path: 'products',
            ref: 'product',
            select: 'productName price _id'
        });
        res.status(200).json(category.products);
    },
    // ------------------Vajalik-------------------- 5 Kustuta toode
    deleteCategory: async (req, res, next) => {
        const category = await Category.findByIdAndRemove(req.value.params.categoryId);
        if (!category) {
            return res.status(404).json ({ error: 'Category doesn\'t exist!'});
        }
        res.status(200).json({ success: true })
    }
};